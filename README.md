# XenoTools

Tools for applying easily configurations into linux systems. Instead of making a basic dotfiles repo with scripts like normal people, I made this autoupdating, universal and customizable tool suite program for my own needs. And possibly everyone else's needs too.

## Usage
You can either use the pre-compiled setup executable or you can run
directly from source from the src/ folder. The binary is the recommended
way since it does not require NodeJS and can be directly executed.

### Dotfiles
You can automate dotfile installation by using the `Configure dotfiles`
functionality. To customize the installation of dotfiles (or whatever files
you may need), create a .xenotools.json file.

#### Available options
| Key | Description |
| --- | ----------- |
| `conditional` | Whether to ask before processing this folder |
| `filemap` | List of files with custom destination. You can either set a custom path, or ignore with a `!`. You can also move all files by default to a new location using the `*` key | 

#### Example .xenotools.json
```
{
  "conditional": true,
  "filemap": {
    "*": "~/things",
    "README.md": "!",
    "myCustomFile": "~/Desktop/myCustomFile",
    "myOtherCustomFile": "/etc/configFile"
  }
}
```