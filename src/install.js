/*
 * The source code for the setup tool.
 * 
 * New feature checklist:
 *  [ ] Add to the 'features' array
 *  [ ] Add to the questionnaire
 *  [ ] Add the feature handler
 *  [ ] Create the actual feature script
 * 
 * Author: Xenomer
 */

const Enquirer = require('enquirer');
var chalk = require('ansi-colors');
var shell = require('shelljs');
var program = require('commander');
var child_process = require('child_process');
var fs = require('fs');
var path = require('path');

var processDotfiles = require('./dotfiles');
var enquirer = new Enquirer();
enquirer.register('suggestions', require('./suggestionPrompt'));

var version = require('./package').version;

// alias for console.log()
var c = (...str) => { console.log(...str); }

var run = (...s) => {
    try { child_process.execFileSync(s[0], s.slice(1), {stdio: 'inherit'}); }
    catch (e) { c(chalk.red('Error running'), s.join(' ')+':', chalk.red(e)); }
}

// the distros supported currently
const supportedOs = [
    'debian',
    'ubuntu',
    'fedora',
    'wsl'
],
features = [
    'node',
    'docker',
    'help',
    'zsh',
    'spaceship',
    'alias-git',
    'alias-node',
    'alias-docker-wsl',
    'alias-tools',
    'nodemon',
    'firebase-tools',
    'create-react-app',
    'pkg',
    'git-config',
    'dotfiles',
    'backup',
]

// parse cmd arguments
program
  .version(version, '-v, --version')
  .option('-f, --features <feature1,feature2>', 'pre select features to be installed. Features: ' + features.join(',') + '.')
  .option('-d, --distro <distro>', 'pre select distro. Currently supported: ' + supportedOs.join(',') + '.')
  .option('-D, --dot-repo <repo>', 'set dotiles repo used when configuring dotfiles.')
  .option('--no-update', 'do not update automatically.')
  .parse(process.argv);

// check if we have the required files in the current directory
// if not, check one folder upwards
process.chdir(path.dirname(process.execPath.endsWith('node') ? __filename : process.execPath));
process.stdout.write('Checking file structure...');
if (!fs.existsSync(path.join(process.cwd(), 'scripts'))) {
    // check one folder upwards (if we are executing from source, src/ folder)
    process.chdir('..');
    if (!fs.existsSync(path.join(process.cwd(), 'scripts'))) {
        c(chalk.red(' Failed\n'));
        c('The required files were not found. Please make sure ' +
          'everything is correct.');
        return;
    }

    // we are running from source!
    else c('', chalk.blue('Running from source'))
}
else c('', chalk.green('Done'))

// check for updates
if(program.update) {
    try {
        child_process.execFileSync('./scripts/self-updater', {stdio: 'inherit', shell:'/bin/bash'});
    } catch (e) {
        if(e.status === 1){
            c('Please restart xenotools.');
            return;
        }
    }
}

// check if root, exit if not
process.stdout.write('Checking root permissions...');
if(!require('is-root')()){
    c(chalk.red(' Failed\n'));
    c(chalk.red("No root access was detected."));
    c(chalk.bold("Please run this with root privileges."));
    return;
}
else c('', chalk.green('Done'))

// set case insensitive distro matching
if(program.distro) program.distro = program.distro.toLowerCase();

// make all files executable automatically
process.stdout.write('Making all scripts runnable...');
shell.exec('sudo chmod -R +x ./scripts', {silent: false, async: false, shell: '/bin/bash', });
c('', chalk.green('Done\n'))

// for running local script files
var runScript = (...s) => { 
    try { child_process.execFileSync('./'+s[0], s.slice(1), {stdio: 'inherit', cwd: './scripts'}); }
    catch (e) { c(chalk.red('Error running'), s.join(' ')+':', chalk.red(e)); }
}

// disclaimer
c('Please beware that ' + 
  chalk.red('this program EXECUTES ALL SCRIPTS WITH ROOT PERMISSIONS') +
  '. You therefore run this program ' +
  'and all it\'s code/scripts ' +
  'at your own risk.\n');

var generateFeatures = () => {
    const isWorkstation = answers['install-type'] === 'Workstation' || answers['install-type'] === 'WSL';
    const isWSL = answers['install-type'] === 'WSL';
    return [
        {role:'separator',message:"-- System --"},
        {name: 'node', message: 'Install Node', hint: 'v8.x', enabled: true },
        {name: 'docker', message: 'Install Docker', enabled: !isWSL, disabled: isWSL?'(Not available for WSL)':false},
        {name: 'help', message: 'Install Help tool', hint: '(allows for cheatsheets and custom text docs)', enabled: isWorkstation},

        {role:'separator',message:"-- Terminal --"},
        {name: 'zsh', message: 'Install ZSH', enabled: isWorkstation },
        {name: 'spaceship', message: 'Install spaceship', hint: '(A prompt for ZSH)', enabled: isWorkstation },

        {role:'separator',message:"-- Aliases --"},
        {name: 'alias-git', message: 'Aliases for Git', enabled: isWorkstation },
        {name: 'alias-node', message: 'Aliases for Node', enabled: isWorkstation },
        {name: 'alias-git', message: 'Aliases for Docker', hint: '(WSL only)', enabled: isWorkstation, disabled: !isWSL },
        {name: 'alias-tools', message: 'Alias for XenoTools', hint: "(use 'xenotools' to invoke)", enabled: true },
        
        {role:'separator',message:"-- Node packages --"},
        {name: 'nodemon', message: 'Install Nodemon', enabled: isWorkstation },
        {name: 'firebase-tools', message: 'Install Firebase-tools', enabled: false },
        {name: 'create-react-app', message: 'Install create-react-app', enabled: false },
        {name: 'pkg', message: 'Install pkg', enabled: false },

        {role:'separator',message:"-- Utilities --"},
        {name: 'git-config', message: 'Configure Git', hint: '(tell Git who you are)', enabled: isWorkstation },
        {name: 'dotfiles', message: 'Automate dotfiles', enabled: isWorkstation },
        {name: 'backup', message: 'Install backup', enabled: false, disabled: '(not available)' },
    ]
}

var answers = {};
if(program.features) {
    answers['install-features'] = program.features.split(',');
}

enquirer.prompt([
    {   // What kind of system?
        type: 'select',
        name: 'install-type',
        message: 'What kind of machine is the installation done to?',
        choices: [
            { name: 'Workstation', message: 'Workstation' },
            { name: 'WSL', message: 'Workstation (WSL)' },
            { name: 'Server', message: 'Server' },
        ],
        skip: () => program.features,
        result: (a) => answers['install-type'] = a
    },
    {   // What distro?
        type: 'select',
        name: 'os',
        message: 'What distro is the installation done to?',
        default: 0,
        choices: [
            'Debian',
            'Ubuntu',
            'Fedora'
        ],
        skip: (a) => program.distro || answers['install-type'] === 'Workstation (WSL)',
        result: (a) => { answers['os'] = a; generateFeatures(); return a; }
    },
    {   // What features?
        type: 'multiselect',
        name: 'install-features',
        message: 'Select installation features',
        choices: generateFeatures,
        skip: () => program.features,
        result: a => answers['install-features'] = answers['install-features'] || a,
    },
    {   // Git information
        type: 'form',
        name: 'git',
        message: 'Enter Git information:',
        choices: [
            { name: 'name', message: 'Name' },
            { name: 'email', message: 'Email' }
        ],
        skip: () => !answers['install-features'].includes('git-config')
    },
    {   // Dotfiles repo
        type: 'suggestions',
        name: 'dotfiles-repo',
        message: 'Enter URL for dotfiles repo:',
        suggestions: [
            'https://gitlab.com/',
            'http://gitlab.com/',
            'https://github.com/',
            'http://github.com/'
        ],
        skip: () => !answers['install-features'].includes('dotfiles') || program.dotRepo,
        result: (a) => (a === 'https://gitlab.com/') ? 'None':a
    }
])
.then(a => {
    var f = a['install-features'];
    if(!f && program.features) {
        f = program.features.split(',');
    }
    if(a['dotfiles-repo'] === 'None') a['dotfiles-repo'] = '';
    if(!a['dotfiles-repo']) a['dotfiles-repo'] = program.dotRepo;

    var os = a['os'] ? a['os'].toLowerCase() : program.distro; // set to selection or argument
    
    // fall back to wsl
    if(!os) { 
        os = 'wsl';
        c('Set os to', os);
    }

    if(!supportedOs.includes(os)){
        c(chalk.red('Invalid distro'), chalk.blue(os));
        return;
    }

    var runFeature = (log, feature, ...script) => { if(f.includes(feature)) { c(chalk.green.bold('\n' + log + '\n')); runScript(...script, os); } }
    var promises = [];
    if(f.includes('dotfiles')) {
        c(chalk.green.bold('\nConfiguring dotfiles\n'));
        promises.push(processDotfiles({
            url: a['dotfiles-repo'],
        }).catch(e => c('Error configuring dotfiles!', e)));
    }
    return Promise.all([{a:a,f:f,os:os,runFeature:runFeature}, ...promises]);
}).then(an => {
    var { a, f, os, runFeature } = an[0];
    // == install the stuff here ==
    runFeature('Installing Node', 'node', 'install-node ');
    runFeature('Installing Docker', 'docker', 'install-docker');
    runFeature('Installing Help', 'help', 'install-help');

    runFeature('Installing ZSH', 'zsh', 'install-zsh');
    runFeature('Installing spaceship-prompt', 'spaceship', 'install-spaceship-prompt');

    runFeature('Applying aliases for git', 'alias-git', 'install-alias-git');
    runFeature('Applying aliases for node', 'alias-node', 'install-alias-node');
    runFeature('Applying aliases for docker (WSL)', 'alias-docker-wsl', 'install-alias-docker-wsl');
    runFeature('Applying aliases for XenoTools', 'alias-tools', 'install-alias-tools', process.execPath);
    
    runFeature('Installing Nodemon', 'nodemon', 'install-npm-package', 'nodemon');
    runFeature('Installing Firebase-tools', 'firebase-tools', 'install-npm-package', 'firebase-tools');
    runFeature('Installing Create-react-app', 'create-react-app', 'install-npm-package', 'create-react-app');
    runFeature('Installing PKG', 'pkg', 'install-npm-package', 'pkg');

    runFeature('Configuring Git', 'git-config', 'configure-git', `${a['git']['name']}`, `${a['git']['email']}`);
    runFeature('Installing backup', 'backup', 'setup-backup');

    // == install the stuff here ==

    c(chalk.green('\nDone!\n'));
}).catch(e => c(chalk.red(e)));
