//
//  Handles the dotfile-processing
//

var { execSync } = require('child_process');
var { prompt }   = require('enquirer');
var fs           = require('fs');
var del          = require('del');
var path         = require('path');
var chalk        = require('ansi-colors');
var promptExpand = require('prompt-expand');

// snippets
var c = (...a) => console.log(...a);
// runs command, returns output if present (output is not returned if using stdio: 'inherit')
var run = (...a) => { var o=execSync(...a);if(o)return o.toString(); };
var clone = (repo, user) => { run(`sudo -u ${user} git clone "${repo}" "${FOLDER}"`, {stdio: 'inherit'}); }
var pull = (user) => { run(`sudo -u ${user} git pull`, {stdio: 'inherit', cwd: 'dotfiles'}); }

// constants
const FOLDER = 'dotfiles';
const CONFIG_FILE = '.xenotools.json';
const IGNORE = [
    '.git',
    CONFIG_FILE
];

module.exports = async(options) => {
    var url = options.url;

    // get user information using bash with root
    options.user_home = run('eval echo ~${SUDO_USER}').replace('\n', '');
    options.user_name = run('logname').replace('\n', '');
    
    var exists = fs.existsSync(`./${FOLDER}`);
    var isGit = fs.existsSync(`./${FOLDER}/.git`);
    /*a = await prompt({
        type: 'toggle',
        name: 'deleteFolder',
        message: `The ${FOLDER} folder already exists. Do you want to remove and reclone it?`,
        enabled: 'Remove and reclone',
        disabled: 'Do nothing',
        skip: () => !exists || !url
    });*/
    var choices = [
        {
            key: 'n',
            name: 'Do nothing and continue',
            value: 'continue'
        },
        new promptExpand.Separator(),
        {
            key: 'x',
            name: 'Abort',
            value: 'abort'
        }
    ];
    if(url) {
        choices.splice(1, 0, {
            key: 'r',
            name: 'Remove and reclone',
            value: 'reclone'
        });
    }
    if(isGit) {
        choices.splice(1, 0, {
            key: 'p',
            name: 'Git pull',
            value: 'pull'
        });
    }
    var p = new promptExpand({
        message: 'The dotfiles folder exists already. What should be done?',
        name: 'exists',
        default: 'x',
        choices: choices
    });
    if (exists && (url || isGit)) {
        var a = await p.run();
        if(a === 'abort') return;
    }

    // git repo URL was not set
    if(!url) {
        if(!exists){
            c('Dotfiles repo is not set. Skipping');
            return;
        }
        else {
            if(isGit && a === 'pull') {
                pull(options.user_name);
            }
        }
    }

    // git repo URL is set
    else {
        // folder exists already, what should we do?
        if(exists) {
            // we want to delete the folder
            if(a === 'reclone') {
                del.sync(`./${FOLDER}/**`)      // remove...
                clone(url, options.user_name);  // ..and reclone it
            }
            else if (a === 'pull') {
                pull(options.user_name);
            }
        }
        // folder does not (yet) exist, continue to clone it
        else {
            clone(url, options.user_name);
        }
    }

    // main processing
    await processFiles(options);
}

async function processFiles(options) {
    c('Processing files...');
    var fullFolderPath = path.join(process.cwd(), FOLDER);
    
    await processFolder(fullFolderPath, options, '');

    c('\nDotfiles processed successfully. Checking aliases');
    run(`./scripts/config-alias --no-create`, {stdio: 'inherit'});
}

var fileMap = {};
var processFolder = async function(dir, o, relative) {
    // read all files and folders in the current folder
    var files = fs.readdirSync(dir);

    // does the folder contain a config file?
    if(files.includes(CONFIG_FILE)) {
        // let's parse the contents of that config file to JSON
        var contents = JSON.parse(fs.readFileSync(path.join(dir, CONFIG_FILE)));

        // 'filemap' node exists
        if(contents.filemap) {
            for (var f in contents.filemap) {
                if (contents.filemap.hasOwnProperty(f)) {
                    const remap = contents.filemap[f];
                    // move all files to remap by default
                    if(f === "*") {
                        files.forEach(i => fileMap[path.join(dir, i)] = path.join(remap.replace('~', o.user_home), i))
                    }
                    else fileMap[path.join(dir, f)] = remap.replace('~', o.user_home);
                }
            }
        }
        
        if(contents.conditional) {
            // we want to ask before processing this folder!
            a = await prompt({
                type: 'toggle',
                name: 'process',
                message: `Process folder '/${relative}'?`,
                enabled: 'Yes',
                disabled: 'No'
            });

            if(!a.process)
                return;
        }
    }
    await _process(dir, o, relative, files);
  };
async function _process(dir, o, relative, files) {
    //loop all files (and folders)
    for (var file of files) {
        var f = fs.statSync(path.join(dir, file));
        var full = path.join(dir, file);

        // ignored, skip
        if(IGNORE.includes(file)){
            c('i', chalk.grey(full));
            continue;
        }

        // directory, loop
        if (f.isDirectory()) {
            c('d', chalk.yellow(full));
            await processFolder(path.join(dir, file), o, path.join(relative, file));
        }

        // file, process
        else if(f.isFile()) {
            // default filemap to ~ + relative
            if(!fileMap[full]) fileMap[full] = path.join(o.user_home, relative, file);

            if(!fileMap[full].startsWith('!')) {
                c('f', chalk.green(full), fileMap[full]);
                try {
                    // create folder structure recursively
                    if(!fs.existsSync(path.dirname(fileMap[full])))
                        fs.mkdirSync(path.dirname(fileMap[full]), {recursive: true});

                    // and finally copy the file itself
                    fs.copyFileSync(full, fileMap[full]);
                }
                catch (e) {
                    c(chalk.red(e));
                }
            }

            // ignored, skip
            else {
                c('i', chalk.grey(full));
            }
        }
    }
}