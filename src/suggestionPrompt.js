/*
    This file contains the SuggestionPrompt for Enquirer.
    It has the ability to support multiple suggestions that
    behave more like suggestions and less like initial text.
*/

const Enquirer = require('enquirer');
const colors = require('ansi-colors');
var enquirer = new Enquirer();

module.exports = class extends Enquirer.StringPrompt {
    constructor(options = {}) {
        super(options);
        this.suggestions = options.suggestions;
    }
    async format(input = this.value) {
        let initial = await this.resolve(this.initial, this.state);
        if (!this.state.submitted) {
            return placeholder(this, { input, initial, pos: this.cursor }, this.suggestions);
        }
        return this.styles.submitted(input || initial);
    }
    next() {
        // finds the most suitable suggestion based on input
        sugg = typeof(this.suggestions) === 'string' ?
            this.suggestions :
            this.suggestions.find(s => s.startsWith(this.value));
        if(!this.input) sugg = undefined;

        if (!sugg || !sugg.startsWith(this.input)) return this.alert();
        this.input = sugg;
        this.cursor = sugg.length;
        this.render();
    }
}

//
//  This is a modified version of the Enquirer internal 'place' method.
//  It works with suggestions instead of the 'internal' property and
//  supports multiple of them.
//
var placeholder = (prompt, options = {}, suggestions) => {
    prompt.cursorHide();

    let { input = '', initial = '', pos, showCursor = true, color } = options;
    let style = color || prompt.styles.placeholder;
    let i = inverse(prompt.styles.primary);
    let blinker = str => i(prompt.styles.black(str));
    let output = input;
    let char = ' ';
    let reverse = blinker(char);
    
    // finds the most suitable suggestion based on input
    sugg = typeof(suggestions) === 'string' ?
        suggestions :
        suggestions.find(s => s.startsWith(input));

    if (prompt.blink && prompt.blink.off === true) {
      blinker = str => str;
      reverse = '';
    }
  
    if (showCursor && pos === 0 && input === '') {
      return blinker(char);
    }

    if (showCursor && pos === 0 && (input === '')) {
      return blinker(initial[0]) + style(initial.slice(1));
    }
    
    // stringify stuff
    initial = `${initial}`;
    input = `${input}`;

    // can we put a suitable placeholder text?
    let placeholder = sugg && sugg.startsWith(input) && sugg !== input && input;
    let cursor = placeholder ? blinker(sugg[input.length]) : reverse;
  
    if (pos !== input.length && showCursor === true) {
      output = input.slice(0, pos) + blinker(input[pos]) + input.slice(pos + 1);
      cursor = '';
    }
  
    if (showCursor === false) {
      cursor = '';
    }
  
    if (placeholder) {
      let raw = prompt.styles.unstyle(output + cursor);
      return output + cursor + style(sugg.slice(raw.length));
    }
  
    return output + cursor;
};
function inverse(color) {
    if (!color || !color.stack) return color;
    let name = color.stack.find(n => colors.keys.color.includes(n));
    if (name) {
        let col = colors['bg' + name[0].toUpperCase() + name.slice(1)];
        return col ? col.black : color;
    }
    let bg = color.stack.find(n => n.slice(0, 2) === 'bg');
    if (bg) {
        return colors[bg.slice(2).toLowerCase()] || color;
    }
    return colors.none;
};