#!/bin/sh

# Executes the program from the upper directory.
# As a downside, this requires Node to be 
# installed and is therefore not the proper
# way to run tools. 
# Please run tools using ./setup from the parent
# directory

cd ..
sudo node src/install.js